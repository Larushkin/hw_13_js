'use strict'

// 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
//      setTimeout()   позволяет вызвать функцию один раз через некотрый промежуток времени
//      setInterval() позволяет вызвать функцию много раз через разный промежуток времени.
// 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
//      Да, сработает. Мгновенно (с задержкой 0 милесекунд), но приоритет выполнения будет ниже.    
// 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
//      clearInterval() останавливает дальнейщее выполнение кода.


const img = document.querySelectorAll(".image-to-show");
const stp = document.querySelector(".stop");
const start = document.querySelector(".start");

let position = 1;
let imgTimerIID = setInterval(showImg, 3000);

function showImg() {
  img.forEach((element, index) => {
    element.classList.add("hidden");
    element.classList.remove("show");
    if (index === position) {
      element.classList.remove("hidden");
      element.classList.add("show");
    }
  });
  if (position < img.length - 1) {
    position++;
  } else position = 0;
}

stp.addEventListener("click", (e) => {
  clearInterval(imgTimerIID);
});

start.addEventListener("click", (e) => {
  imgTimerIID = setInterval(showImg, 3000);
  
});


